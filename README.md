# Theme for presentations

This theme is based on [reveal.js](https://revealjs.com/) and adapted for presentations given under the R3 umbrella.

## How to get started

Include this theme as a submodule next to your presentation.
